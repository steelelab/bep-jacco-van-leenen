#include <stdio.h>
#include <iostream>
#include <iomanip>

#include <cassert>
#include <string.h>
#include <unistd.h>
#include <cstdint>

#include <pocketvna.h>

#include <vector>
#include <functional>

#include <cmath>
#include <complex>

#include "device.h"
#include "device-enumerator.h"

#include "predefines.h"


///> Check driver version. It is not dependant on handler
void check_version() {
    uint16_t ver = 0;
    double pi = 0.;

    assert( PVNA_Res_Ok == pocketvna_driver_version(&ver, &pi) );

    std::cout << "CHECK driver_version call OK" << std::endl;
    std::cout << "\tVERSION = '" << ver << "'" << std::endl;

    if ( std::abs(Pi - pi) > 1.E-10 ) {
        std::cerr << "\tPI IS INVALID: " << pi << std::endl;
        exit(EXIT_FAILURE);
    } else {
        std::cout << "\tPI IS OK" << std::endl;
    }
}

void printDevices(DeviceEnumerator &);
void runCalibrationExample();
void runOpenDeviceAndScanExample(PVNA_DeviceDesc &);

void runExamples() {
    DeviceEnumerator enumerator;

    if (! enumerator.any() ) {
        std::cerr << "NO PocketVna device is connected" << std::endl;
        return;
    }

    printDevices(enumerator);

    runOpenDeviceAndScanExample(enumerator.list[0]);

    runCalibrationExample();
}

int main(int, char * []) {
    check_version();

    runExamples();

    pocketvna_close();

    return 0;
}



void printDevices(DeviceEnumerator & enumerator) {
    std::cout << "DETECTED DEVICES: " << std::endl;
    for ( unsigned i = 0; i < enumerator.size; ++i ) {
        auto desc = enumerator.list[i];
        std::cout << i << ") "
                  << "\tManufacturer: " << std::wstring(desc.manufacturer_string)
                  << ", Product: " << std::wstring(desc.product_string)
                  << ", S/N: " << std::wstring(desc.serial_number)
                  << ", VER: " << desc.release_number
                  << ", PATH: '" << desc.path << "'"
                  << ", PERMISSIONS: " << ( desc.access & PVNA_ReadAccess ? "R" : "") << (desc.access & PVNA_WriteAccess ? "W" : "")
                  << std::endl;
    }
}


