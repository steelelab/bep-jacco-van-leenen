#pragma once

#include <pocketvna.h>
#include <string>
#include <stdexcept>
#include <cassert>

///> RAII for enumerator. Lists available devices and frees list after use
struct DeviceEnumerator {
    PVNA_DeviceDesc * list;
    uint16_t size;

    DeviceEnumerator():list(nullptr), size(0) {
        PVNA_Res r;
        if ( PVNA_Res_Ok != (r = pocketvna_list_devices(&list, &size)) ) {
            throw std::runtime_error("bad pocketvna_list_devices: " + std::to_string((unsigned)r));
        }
    }

    ~DeviceEnumerator() {
        if ( list ) {
            assert(PVNA_Res_Ok == pocketvna_free_list(&list)); //ONLY OK STATUS IS POSSIBLE
            assert(list == nullptr); //should zero list
        }
    }

    bool any() const noexcept {  return list != nullptr && size > 0; }

    DeviceEnumerator(const  DeviceEnumerator &)             = delete;
    DeviceEnumerator(DeviceEnumerator &&)                   = delete;

    DeviceEnumerator& operator =(const  DeviceEnumerator &) = delete;
    DeviceEnumerator& operator =(DeviceEnumerator &&)       = delete;
};
