#include <pocketvna.h>
#include "predefines.h"
#include "device.h"

void printParam(const Params & );
void printNetwork(const Network & );
void performScan(Device & device, unsigned networkParameter);
void performScanAndCancelIt(Device & device);
void checkBindingWorks(Device & device);

void runOpenDeviceAndScanExample(PVNA_DeviceDesc & desc) {
    try {
        Device device( std::unique_ptr<DeviceHandler>(new DeviceHandler(&desc)) );
        auto props = device.getProperties();

        std::cout << "Device Properties:" << std::endl
                  << "\tZ0: " << props.z0 << std::endl
                  << "\tValid Frequency: " << props.validRange << std::endl
                  << "\tReasonable Frequency: " << props.reasonableRange << std::endl
                  << "\tSupported: " << (props.isS11 ? " S11 " : "") <<  (props.isS21 ? " S21 " : "")
                  << (props.isS12 ? " S12 " : "") << (props.isS22 ? " S22 " : "") << std::endl << std::endl;

        std::cout << "Check if binding is correct" << std::endl;
        checkBindingWorks(device);

        std::cout << "SINGLE POINT FULL SCAN" << std::endl;
        printParam( device.scan(500_KHz, 5, PVNA_ALL) );

        std::cout << "SINGLE POINT S22 SCAN" << std::endl;
        printParam( device.scan(2_MHz, 5, PVNA_S22) );

        std::cout << "FULL NETWORK SCAN: " << std::endl;
        performScan(device, PVNA_ALL);

        std::cout << "S21 only SCAN: " << std::endl;
        performScan(device, PVNA_S21);

        std::cout << "S11 and S21 only SCAN: " << std::endl;
        performScan(device, PVNA_FORWARD);

        std::cout << "Cancelable scan: " << std::endl;
        performScanAndCancelIt(device);

    } catch ( const ScanCanceled & ) {
        std::cerr << "Scan is stopped manually" << std::endl;
    } catch ( const  DeviceGone & ) {
        std::cerr << "Device is disconnected" << std::endl;
    }
}

void printParam(const Params & ntwk )  {
    std::cout << std::setprecision(5);

        std::cout << ntwk.freq << ": \n"
                  << '\t' << ntwk.s11 << " \t" << ntwk.s21 << std::endl
                  << '\t' << ntwk.s12 << " \t" << ntwk.s22 << std::endl;

}

void printNetwork(const Network & ntwk )  {
    std::cout << std::setprecision(5);

    for ( size_t i = 0; i < ntwk.frequency.size(); ++i ) {
        std::cout << ntwk.frequency[i] << ": \n"
                  << '\t' << ntwk.s11[i] << " \t" << ntwk.s21[i] << std::endl
                  << '\t' << ntwk.s12[i] << " \t" << ntwk.s22[i] << std::endl;
    }
}

void performScan(Device & device, unsigned networkParameter) {
    FrequencyArray freqs = { 2_MHz, 3_MHz, 4_MHz, 5_MHz, 6_MHz, 7_MHz, 8_MHz, 9_MHz, 10_MHz };
    size_t indexTracker = 0;

    auto network = device.scan(freqs, 3, networkParameter, [&](size_t index) {
        indexTracker = index;
        std::cout << "Currently scanned index is: " << index << "\r" << std::flush;
        return true;
    });

    assert( indexTracker == freqs.size() );

    printNetwork(network);
}

void performScanAndCancelIt(Device & device) {
    FrequencyArray freqs = { 2_MHz, 3_MHz, 4_MHz, 5_MHz, 6_MHz, 7_MHz, 8_MHz, 9_MHz, 10_MHz };

    auto network = device.scan(freqs, 3, PVNA_S11, [&](size_t index) {
        if ( index >= 5 ) return false;
        return true;
    });

    printNetwork(network);
}

void checkBindingWorks(Device &device) {
    device.assertBindingWorks();


}
