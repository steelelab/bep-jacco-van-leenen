#include <stdio.h>
#include <iostream>
#include <iomanip>

#include <cassert>
#include <string.h>
#include <unistd.h>

#include "pocketvna.h"

#include <vector>
#include <functional>

#include <cmath>

//#include <thread>
//LINK AGAINST STATIC hid-api
constexpr long double Pi { 3.141592653589793238462643383279502884L };
inline constexpr  unsigned long long operator "" _Hz(unsigned long long v) { return v; }

inline constexpr unsigned long long operator "" _KHz(unsigned long long v) { return v * 1000u; }

inline constexpr unsigned long long operator "" _MHz(unsigned long long v) { return v * 1000u * 1000u; }

inline constexpr unsigned long long operator "" _GHz(unsigned long long v) { return v * 1000u * 1000u * 1000u; }

std::ostream & operator<<(std::ostream & out, const std::wstring & ws ) {
    std::wcout << ws << std::flush;
    return out;
}

struct ProgressListener {
    std::function<void(size_t)> proc;

    void operator()(size_t i) const {  proc(i);  }

    static void listener(PVNA_UserDataPtr data, uint32_t count) {
        ProgressListener & l = *(reinterpret_cast<ProgressListener*>(data));
        l(count);
    }

    PVNA_ProgressCallBack toProc() {
        return {
            reinterpret_cast<PVNA_UserDataPtr>(this),
            listener
        };
    }
};

struct DeviceEnumerator {
    PVNA_DeviceDesc * list;
    uint16_t size;

    DeviceEnumerator():list(nullptr), size(0) {
        PVNA_Res r;
        if ( PVNA_Res_Ok != (r = pocketvna_list_devices(&list, &size)) ) {
            throw std::runtime_error("bad pocketvna_list_devices: " + std::to_string((unsigned)r));
        }
    }
    ~DeviceEnumerator() {
        if ( list ) {
            //ONLY OK STATUS IS POSSIBLE
            assert(PVNA_Res_Ok == pocketvna_free_list(&list));
            assert(list == nullptr);
        }
    }
    bool any() const noexcept {  return list != nullptr && size > 0; }

    DeviceEnumerator(const  DeviceEnumerator &) = delete;
    DeviceEnumerator(DeviceEnumerator &&) = delete;

    DeviceEnumerator& operator =(const  DeviceEnumerator &) = delete;
    DeviceEnumerator& operator =(DeviceEnumerator &&) = delete;
};

struct DeviceHandler {
    PVNA_DeviceHandler handle {nullptr};

    DeviceHandler(PVNA_DeviceDesc * desc) {
        PVNA_Res r;
        if ( PVNA_Res_Ok != (r = pocketvna_get_device_handle_for(desc, &handle)) ) {
            assert(handle == nullptr);
            throw std::runtime_error("can not connect to device. pocketvna_get_device_handle_for: " + std::to_string((unsigned)r));
        }
    }
    ~DeviceHandler() {
        if ( handle ) {
            pocketvna_release_handle(&handle);
            assert(handle == nullptr);
        }
    }
    DeviceHandler(const  DeviceHandler &) = delete;
    DeviceHandler(DeviceHandler &&) = delete;

    DeviceHandler& operator =(const  DeviceHandler &) = delete;
    DeviceHandler& operator =(DeviceHandler &&) = delete;

};

void check_version() {
    uint16_t ver = 0;
    double pi = 0.;
    assert( PVNA_Res_Ok == pocketvna_driver_version(&ver, &pi) );

    std::cout << "CHECK driver_version call OK" << std::endl;
    std::cout << "\tVERSION = '" << ver << "'" << std::endl;
    if ( std::abs(Pi - pi) > 1.E-6 ) {
        std::cerr << "\tPI IS INVALID: " << pi << std::endl;
        exit(EXIT_FAILURE);
    } else {
        std::cout << "\tPI IS OK" << std::endl;
    }
}

void check_supported_network_parameters(DeviceHandler & handle) {
    auto is_trans_supported = [&handle](PVNA_NetworkParam p) -> bool {
        auto r = pocketvna_is_transmission_supported(handle.handle, p);
        if ( r == PVNA_Res_Ok ) return true;
        if ( r == PVNA_Res_UnsupportedTransmission ) return false;

        if ( r == PVNA_Res_InvalidHandle )
            throw std::runtime_error("Looks kind of Device is disconnected");

        throw std::runtime_error("pocketvna_is_transmission_supported returned unexpected value: " + std::to_string(r));
    };
    std::cout << "SUPPORTED Network Params: ";
    if ( is_trans_supported(PVNA_S11) ) std::cout << " S11 ";
    if ( is_trans_supported(PVNA_S21) ) std::cout << " S21 ";
    if ( is_trans_supported(PVNA_S12) ) std::cout << " S12 ";
    if ( is_trans_supported(PVNA_S22) ) std::cout << " S22 ";
    std::cout << "   OK " << std::endl;
}

void check_settings(DeviceHandler & handle) {
    PVNA_Frequency from = 0, to = 0;
    auto r = pocketvna_get_valid_frequency_range(handle.handle, &from, &to);
    assert(r == PVNA_Res_Ok);
    assert(from == 1_Hz && to == 6_GHz);

    from = to = 0;
    r = pocketvna_get_reasonable_frequency_range(handle.handle, &from, &to);
    assert(r == PVNA_Res_Ok);
    assert(from >= 1_Hz && from < 6_GHz &&
           to > 1_Hz    && to <= 6_GHz &&
           from < to );

    double R = 0.0;
    r = pocketvna_get_characteristic_impedance(handle.handle, &R);
    assert(r == PVNA_Res_Ok);
    assert( std::abs(R - 50.) <= 1E-4 );
}

void check_debug_request(DeviceHandler & handle, unsigned & inner_buffer_size) {
    std::vector<uint8_t> response(10, 0x0);
    uint32_t szxx = response.size();
    std::cout << "DEBUG REQUEST: ";
    PVNA_Res r = pocketvna_debug_request(handle.handle, response.data(), &szxx);
    if ( r == PVNA_Res_Ok ) {
        std::cout << " OK! " << szxx << " bytes read: [ ";
        for ( size_t i = 0; i < response.size(); ++i ) {
            std::cout << std::hex << "0x" << int(response[i]) << ", ";
        }
        std::cout << " ] " << std::dec << std::endl;

        assert(response[0] == 0);
        assert(response[1] == 3);
        assert(response[3] == 8);
        assert(response[4] = 13);
        assert(response[7] = 8);

        inner_buffer_size = (uint32_t(response[5]) << 8) | (uint32_t(response[6]) & 0xFF);
        std::cout << "INNER BUFFER SIZE : " << inner_buffer_size << std::endl;

        assert(inner_buffer_size > 0 );
    } else {
        std::cerr << "BAD DEBUG REQUEST: " << r << std::endl;
        exit(EXIT_FAILURE);
    }
}

void check_read_device_buffer(DeviceHandler & handle, unsigned inner_buffer_size) {
    std::vector<uint8_t> buffer(200, 255);
    uint32_t sz = buffer.size();

    std::cout << "------------------------------" << std::endl;

    std::cout << "READ MEMORY " << std::endl;
    PVNA_Res r = pocketvna_debug_read_buffer(handle.handle, buffer.data(), &sz);

    assert(buffer.size() > inner_buffer_size ? inner_buffer_size == sz : sz == buffer.size());

    if ( r == PVNA_Res_Ok ) {
        std::cout << "Buffer:  " <<  std::dec << sz << " - ";
        for ( size_t i = 0; i < buffer.size(); ++i ) {
            std::cout << std::dec <<  int(buffer[i]) << ", ";
        }
        std::cout << std::endl;
    } else {
        std::cerr << "BAD MEMORY BUFFER REQUEST pocketvna_debug_read_buffer: " << r << std::endl;
        exit(EXIT_FAILURE);
    }

    std::cout << "------------------------------" << std::endl;
}

void check_single_scan(DeviceHandler & handle) {
    std::cout << "S11 ONLY SCAN: " << std::endl;

    PVNA_Res r = PVNA_Res_Ok;

    PVNA_Sparam s11 = {-1, -1};
    PVNA_Sparam s21 = {-1, -1};
    PVNA_Sparam s12 = {-1, -1};
    PVNA_Sparam s22 = {-1, -1};

    r = pocketvna_single_query(handle.handle, 10_MHz, 5, PVNA_S11, &s11, nullptr, nullptr, nullptr);
    if ( r != PVNA_Res_Ok ) {
        std::cerr << "FAILED: " << r << std::endl;
        assert(r == PVNA_Res_Ok);
    }

    assert(s11.real != -1 && s11.imag != -1);
    std::cout << "-- OK " << std::endl;

    std::cout << "S11 ONLY SCAN not modify others parammeters passed: " << std::endl;
    s11 = {-1, -1};
    r = pocketvna_single_query(handle.handle, 10_MHz, 5, PVNA_S11, &s11, &s21, &s12, &s22);

    if ( r != PVNA_Res_Ok ) {
        std::cerr << "FAILED: " << r << std::endl;
        assert(r == PVNA_Res_Ok);
    }
    assert(s11.real != -1 && s11.imag != -1);
    assert(s21.real == -1 && s21.imag == -1);
    assert(s12.real == -1 && s12.imag == -1);
    assert(s22.real == -1 && s22.imag == -1);
    std::cout << "-- OK" << std::endl;

    std::cout << "S21 ONLY SCAN: " << std::endl;
    s11 = {-1, -1};
    r = pocketvna_single_query(handle.handle, 10_MHz, 5, PVNA_S21, nullptr, &s21, nullptr, nullptr);
    if ( r != PVNA_Res_Ok ) {
        std::cerr << "FAILED: " << r << std::endl;
        assert(r == PVNA_Res_Ok);
    }
    assert(s21.real != -1 && s21.imag != -1);
    std::cout << "-- OK " << std::endl;

    std::cout << "S21 ONLY SCAN not modify others parammeters passed: " << std::endl;
    s21 = {-1, -1};
    r = pocketvna_single_query(handle.handle, 10_MHz, 5, PVNA_S21, &s11, &s21, &s12, &s22);
    if ( r != PVNA_Res_Ok ) {
        std::cerr << "FAILED: " << r << std::endl;
        assert(r == PVNA_Res_Ok);
    }
    assert(s11.real == -1 && s11.imag == -1);
    assert(s21.real != -1 && s21.imag != -1);
    assert(s12.real == -1 && s12.imag == -1);
    assert(s22.real == -1 && s22.imag == -1);
    std::cout << "-- OK" << std::endl;

    {
    std::cout << "FULL READ not modify others parammeters passed: " << std::endl;
    PVNA_Sparam s11 = {-1, -1};
    PVNA_Sparam s21 = {-1, -1};
    PVNA_Sparam s12 = {-1, -1};
    PVNA_Sparam s22 = {-1, -1};
    r = pocketvna_single_query(handle.handle, 10_MHz, 5,
                               PVNA_NetworkParam(PVNA_S11|PVNA_S21|PVNA_S12|PVNA_S22),
                               &s11, &s21, &s12, &s22);
    if ( r != PVNA_Res_Ok ) {
        std::cerr << "FAILED: " << r << std::endl;
        assert(r == PVNA_Res_Ok);
    }
    assert(s11.real != -1 && s11.imag != -1);
    assert(s21.real != -1 && s21.imag != -1);
    assert(s12.real != -1 && s12.imag != -1);
    assert(s22.real != -1 && s22.imag != -1);
    std::cout << "-- OK" << std::endl;
    }

    {
    PVNA_Sparam s11 = {-1, -1};
    PVNA_Sparam s21 = {-1, -1};
    PVNA_Sparam s12 = {-1, -1};
    PVNA_Sparam s22 = {-1, -1};
    std::cout << "PASS LOW FREQUENCY: " << std::endl;
    r = pocketvna_single_query(handle.handle, 0_Hz, 5,
                               PVNA_NetworkParam(PVNA_S11|PVNA_S21|PVNA_S12|PVNA_S22),
                               &s11, &s21, &s12, &s22);
    assert(PVNA_Res_BadFrequency == r);
    assert(s11.real == -1 && s11.imag == -1);
    assert(s21.real == -1 && s21.imag == -1);
    assert(s12.real == -1 && s12.imag == -1);
    assert(s22.real == -1 && s22.imag == -1);
    std::cout << "-- OK" << std::endl;
    std::cout << "PASS TOO HIGH FREQUENCY: " << std::endl;
    r = pocketvna_single_query(handle.handle, 6_GHz + 1, 5,
                               PVNA_NetworkParam(PVNA_S11|PVNA_S21|PVNA_S12|PVNA_S22),
                               &s11, &s21, &s12, &s22);
    assert(PVNA_Res_BadFrequency == r);
    assert(s11.real == -1 && s11.imag == -1);
    assert(s21.real == -1 && s21.imag == -1);
    assert(s12.real == -1 && s12.imag == -1);
    assert(s22.real == -1 && s22.imag == -1);
    std::cout << "-- OK" << std::endl;
    }

}

void check_multi_scan(DeviceHandler & handle) {
    unsigned counterx = 0;
    unsigned calls = 0;
    ProgressListener list{ [&counterx, &calls](size_t i) { counterx = i; calls ++; } };
    auto proc = list.toProc();

    std::vector<PVNA_Frequency> freq(40, 0);
    const PVNA_Sparam def {-1., -1.};
    std::vector<PVNA_Sparam> s11(freq.size(), def), s21(freq.size(), def),
                             s12(freq.size(), def), s22(freq.size(), def);

    for ( size_t i = 0; i < freq.size(); ++i ) freq[i] = (i + 1) * 1000 * 1000;

    PVNA_Res r = pocketvna_multi_query(handle.handle, freq.data(), freq.size(), 4, PVNA_NetworkParam(PVNA_S11|PVNA_S21|PVNA_S12|PVNA_S22),
                          s11.data(), s21.data(), s12.data(), s22.data(), &proc);
    assert(r == PVNA_Res_Ok);
    assert(freq.size() == counterx);
    assert(calls > 1 && calls <= counterx + 1);

    std::cout << std::setprecision(5);

    for ( size_t i = 0; i < freq.size(); ++i ) {
        std::cout << freq[i] << ": \n"
                  << '\t' << s11[i].real << " + "  << s11[i].imag << "i; \t"
                          << s21[i].real << " + "  << s21[i].imag << "i; " << std::endl
                  << '\t' << s12[i].real << " + "  << s12[i].imag << "i; \t"
                          << s22[i].real << ", "  << s22[i].imag << "; "
                  << std::endl;
    }
}

int main(int, char * []) {
    check_version();

    DeviceEnumerator enumerator;

    if (! enumerator.any() ) {
        std::cerr << "NO PocketVna device is connected" << std::endl;
        return 1;
    }

    std::cout << "DETECTED DEVICES: " << std::endl;
    for ( unsigned i = 0; i < enumerator.size; ++i ) {
        auto desc = enumerator.list[i];
        std::cout << i << ") "
                  << "\tManufacturer: " << std::wstring(desc.manufacturer_string)
                  << ", Product: " << std::wstring(desc.product_string)
                  << ", S/N: " << std::wstring(desc.serial_number)
                  << ", VER: " << desc.release_number
                  << ", PATH: '" << desc.path << "'"
                  << ", PERMISSIONS: " << ( desc.access & PVNA_ReadAccess ? "R" : "") << (desc.access & PVNA_WriteAccess ? "W" : "")
                  << std::endl;
    }
    std::cout << "OK" << std::endl;


    auto & list = enumerator.list;
    auto & sz = enumerator.size;

    for ( size_t i = 0; i < sz; ++i ) {
            std::cout << i << ") PATH: '" << list[i].path << "', "
                      << ". MANUFACTURER: " ;
            std::wcout <<  list[i].manufacturer_string
                       << L". PRODUCT: " << list[i].product_string
                       << L". S/N: " << list[i].serial_number
                       << L". RN: "  << list[i].release_number << std::endl;

            if ( list[i].access & PVNA_ReadAccess )
                std::cout << " R ";
            if ( list[i].access & PVNA_WriteAccess )
                std::cout << " W ";

            DeviceHandler handle(&list[i]);

            check_supported_network_parameters(handle);

            check_settings(handle);

            unsigned inner_buffer_size = 0;
            check_debug_request(handle, inner_buffer_size);
            check_read_device_buffer(handle, inner_buffer_size);

            check_single_scan(handle);

            ////                    PocketVnaResult r1 = pocketvna_enter_dfu_mode(handle);
            ////                    std::cout << "ENTER DFU MODE: " << r1 << std::endl;

            check_multi_scan(handle);

        }

    return 0;
}



