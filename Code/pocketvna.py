"""@package pocketvna
Python binding for pocketvna API
# @file
#  @defgroup API pocketVna API
"""

#import ctypes
from ctypes import *
import os
import sys
import numpy
import platform

def priv_architecture_():
    arch, maxsize  = platform.architecture()[0], sys.maxsize
    if arch.startswith('32') and not (maxsize > 2 ** 32):
        return '_x32'
    elif arch.startswith('64') and (maxsize > 2 ** 32):
        return '_x64'
    else:
        return ''

def priv_get_os_():
    if os.name == 'nt' or sys.platform.startswith('win32'):
        return "windows"
    elif sys.platform.startswith('linux'):
        return "linux"
    elif sys.platform.startswith('darwin'):
        return "macos"
    else:
        return None

BaseName = 'PocketVnaApi'
ARCH     = priv_architecture_()
OS       = priv_get_os_()

def priv_find_file_(filename, thispath):
    for fn in os.listdir(os.path.join(thispath)):
        if (filename in fn) and ( (".so" in fn) or (".dll" in fn) or (".dylib" in fn) ):
            return os.path.join(thispath, fn)
    return None    

def priv_load_library_(orig_fn):
    global PVNA
    pocketvna_path = os.path.dirname(os.path.abspath(__file__))
    try:
        PVNA = CDLL(orig_fn)
        return PVNA
    except OSError:
        another_try = priv_find_file_('{}{}'.format(BaseName, ARCH), pocketvna_path)
        if another_try is None:
            another_try = priv_find_file_('{}'.format(BaseName), pocketvna_path)

        if another_try is None:
            raise

        print('Could not load "{}" by systempath. Trying another one "{}"'.format(orig_fn, another_try))
        
        PVNA = CDLL(another_try)
        return PVNA



def priv_get_lib_name_():    
    if OS == "windows":
        return '{}{}'.format(BaseName, ARCH)
    elif OS == 'linux':
        return 'lib{}{}.so'.format(BaseName, ARCH)
    elif OS == 'macos':
        return 'lib{}{}.dylib'.format(BaseName, ARCH)
    else:
        return '{}{}'.format(BaseName, ARCH)

pocketvna = priv_load_library_(priv_get_lib_name_())

PVNA = pocketvna


# typedef void* PVNA_UserDataPtr;
# typedef void (*PVNA_PFN_Progress_Proc) (PVNA_UserDataPtr, uint32_t);
# typedef struct PocketVNaProgressCallBack 


def inc():
    global EnumCount

    if EnumCount is None:
        EnumCount = 0 

    v = EnumCount
    EnumCount = EnumCount + 1

    return v

EnumCount = 0

class Result:
    OK            = inc()
    NoDevice      = inc()
    NoMemory      = inc()
    InitError     = inc()
    BadDescriptor = inc()

    DeviceLocked  = inc()

    BadDevicePath = inc()
    AcessDenied   = inc()
    CouldNotOpen  = inc()
    InvalidHandle = inc()

    BadTrans      =inc()
    UnsupportedTrans=inc()
    BadFrequency  = inc()
    ReadFailure   =inc()
    EmptyResponse =inc()
    IncompleteResponse=inc()
    WriteFailure  =inc()
    ArrayTooBig   =inc()
    BadResponse   =inc()

    RESP_Unused   =inc()

    DEV_UnknownMode =inc()
    DEV_UnknownParam=inc()
    DEV_NoInit      =inc()
    DEV_HighFreq    =inc()
    DEV_LowFreq     =inc()
    DEV_OutOfBound  =inc()
    DEV_UnknownVar  =inc()
    DEV_UnknownError=inc()
    DEV_BadFormat   =inc()

class Access:
    Denied = 0x00
    Read   = 0x01
    Write  = 0x02

class NetworkParams:
    Non   = 0x00
    S21   = 0x01
    S11   = 0x02
    S12   = 0x04
    S22   = 0x08
    ALL   = S11 | S21 | S12 | S22
    PORT1 = S11 | S21
    PORT2 = S22 | S12
    REFL  = S11 | S22
    TRANS = S21 | S12



class DeviceDescriptor(Structure):
    _fields_ = [
                ("path",       c_char_p),   # Path to device
                ("access",     c_uint32),   # Flags whether we have R or W rights. Useful on Linux
                ("sn",         c_wchar_p),  # Serial Number
                ("vendor",     c_wchar_p),  #
                ("product",    c_wchar_p),
                ("version",    c_uint16),
                ("pid",        c_uint16),
                ("vid",        c_uint16),
                ("aux",        c_uint16),
                ("next",       c_void_p)
     ]

class Priv_SParam(Structure):
    _fields_ = [
        ("real", c_double),
        ("imag", c_double)
    ]

DeviceHandler = c_void_p
DeviceDescriptorList = POINTER(DeviceDescriptor)
ReturnType           = c_uint
DeviceHandlerPtr     = POINTER(DeviceHandler)
NetworkParam         = c_uint
Frequency            = c_uint64
FrequencyPtr         = POINTER(Frequency)
Average              = c_uint16
Version              = c_uint16
Priv_SParamRef            = POINTER(Priv_SParam)
Priv_SParamArray          = (POINTER(c_double))

class PocketVnaError(RuntimeError):
    def __init__(self, message):
        super(RuntimeError, self).__init__(message)

class PocketVnaAPIError(PocketVnaError):
    def code(self):
        return self.error_code

    def __init__(self, message, error_code):
        super(PocketVnaError, self).__init__(message)

        # Now for your custom code...
        self.error_code = error_code

class PocketVnaUnexpectedBehavior(PocketVnaError):
    def __init__(self, message):
        super(PocketVnaError, self).__init__(message)

class PocketVnaHandlerInvalid(PocketVnaError):
    def __init__(self):
        super(PocketVnaError, self).__init__("Handler is not valid or device has been gone") 

class PocketVnaAccessDenied(PocketVnaError):
    def __init__(self, additional):
        if OS == "linux":
            super(PocketVnaError, self).__init__('Access Denied. {}. {}. {}. ({})'.format(
                "Probably you have no RW Permissions to use the device",
                "Try to create *.rules file, Or modify permissions with chmod, or use sudo",
                "Main sign of this problem is you see the device its SN,Product so on but can not connect",
                        additional))
        else:
            super(PocketVnaError, self).__init__("Access Denied. For some reason you have no permission to connect to device")

def check_OK(code, message):
    if code != Result.OK:
        if code == Result.InvalidHandle:
            raise PocketVnaHandlerInvalid()
        if code == Result.AcessDenied:
            raise PocketVnaAccessDenied(':(')
        raise PocketVnaAPIError(message, code)


## ------------- Connect stuff ----------------------------------


# PVNA_Res   pocketvna_driver_version(uint16_t * version, double *info);
PVNA.pocketvna_driver_version.argtypes = [POINTER(Version), POINTER(c_double)]
PVNA.pocketvna_driver_version.restype  = ReturnType

def driver_version():
    p1 = Version (0)
    p2 = c_double (0.0)

    r = PVNA.pocketvna_driver_version(byref(p1), byref(p2))

    check_OK( r, "Could not get Driver Version" )

    return p1.value, p2.value

# PVNA_EXPORTED PVNA_Res   pocketvna_close();
PVNA.pocketvna_close.restype  = ReturnType

def close_api():
    r = PVNA.pocketvna_close()
    check_OK( r, "Close API should be always OK" )

# PVNA_Res   pocketvna_list_devices(PVNA_DeviceDesc ** list, uint16_t * size);
PVNA.pocketvna_list_devices.argtypes = [POINTER(DeviceDescriptorList), POINTER(c_uint16)]
PVNA.pocketvna_list_devices.restype  = ReturnType
    
def list_devices():
    lst = DeviceDescriptorList()
    sz = c_uint16(0)    

    r = PVNA.pocketvna_list_devices(byref(lst), byref(sz))

    if r == Result.NoDevice: return [], 0

    check_OK(r, "Failed to enumerate devices")

    return lst, sz.value

# PVNA_Res   pocketvna_free_list(PVNA_DeviceDesc ** list);
PVNA.pocketvna_free_list.argtypes = [POINTER(DeviceDescriptorList)]
PVNA.pocketvna_free_list.restype  = ReturnType
    
def free_list(lst):
    r = PVNA.pocketvna_free_list(byref(lst))

    check_OK(r, "Failed to free_list")

    if bool(lst):
        raise PocketVnaUnexpectedBehavior("free_list should Zero a reference. API returned OK status but the reference is not NULL") 


## ------------- Connect stuff ----------------------------------

#     PVNA_Res   pocketvna_get_device_handle_for(const PVNA_DeviceDesc* desc, PVNA_DeviceHandler * handle)
PVNA.pocketvna_get_device_handle_for.argtypes = [POINTER(DeviceDescriptor), DeviceHandlerPtr]
PVNA.pocketvna_get_device_handle_for.restype  = ReturnType

def get_device_handler(desc_ptr):
    h = DeviceHandler()
    r = PVNA.pocketvna_get_device_handle_for(desc_ptr, byref(h))

    check_OK(r, "Failed to get device handler")
    
    if not bool(h):
        raise PocketVnaUnexpectedBehavior("Handler is invalid. API reported Ok status but handler is invalid")

    return h

#     PVNA_Res   pocketvna_release_handle(PVNA_DeviceHandler * handle)
PVNA.pocketvna_release_handle.argtypes = [DeviceHandlerPtr]
PVNA.pocketvna_release_handle.restype  = ReturnType
def release_handler(handler):
    r = PVNA.pocketvna_release_handle(byref(handler))

    check_OK(r, "Failed to release handler")
    
    if bool(handler):
        raise PocketVnaUnexpectedBehavior("release_handler should Zero a reference. API returned Ok status but handler is not NULL")

#     PVNA_Res   pocketvna_is_transmission_supported(const PVNA_DeviceHandler handle, const PVNA_NetworkParam param);
PVNA.pocketvna_is_transmission_supported.argtypes = [DeviceHandler, NetworkParam]
PVNA.pocketvna_is_transmission_supported.restype  = ReturnType

def supports_network_parameter(handler, param):
    r = PVNA.pocketvna_is_transmission_supported(handler, param)

    if r == Result.UnsupportedTrans:
        return False

    if r == Result.OK:
        return True

    check_OK(r, "Failed to ask whether a transmission supported")

    return None

#     PVNA_Res   pocketvna_is_valid(const PVNA_DeviceHandler handle);
PVNA.pocketvna_is_valid.argtypes = [DeviceHandler]
PVNA.pocketvna_is_valid.restype  = ReturnType

def is_valid(handler):
    if not bool(handler): return None

    r = PVNA.pocketvna_is_valid(handler)

    if r == Result.InvalidHandle:
        return False

    if r == Result.OK:
        return True

    check_OK(r, "Failed to check whether the handler is valid")

    return None

#     PVNA_Res   pocketvna_version(const PVNA_DeviceHandler handle, uint16_t * version);
PVNA.pocketvna_version.argtypes = [DeviceHandler, POINTER(Version)]
PVNA.pocketvna_version.restype  = ReturnType
    
def get_version(handle):    
    ver = Version(0)    

    r = PVNA.pocketvna_version(handle, byref(ver))

    check_OK(r, "Failed to get a firmware version")

    return ver.value

#     PVNA_Res   pocketvna_get_characteristic_impedance(const PVNA_DeviceHandler handle, double * R);
PVNA.pocketvna_get_characteristic_impedance.argtypes = [DeviceHandler, POINTER(c_double)]
PVNA.pocketvna_get_characteristic_impedance.restype  = ReturnType
    
def get_characteristic_impedance(handle):    
    rr = c_double(0.0)    

    r = PVNA.pocketvna_get_characteristic_impedance(handle, byref(rr))

    check_OK(r, "Failed to get a Characteristic Impedance (aka Reference Resistance/Impedance, Zero Resistance/Impedance)")
    

    return rr.value

#  PVNA_Res   pocketvna_get_valid_frequency_range(const PVNA_DeviceHandler handle, PVNA_Frequency * from, PVNA_Frequency *to);
PVNA.pocketvna_get_valid_frequency_range.argtypes = [DeviceHandler, FrequencyPtr, FrequencyPtr]
PVNA.pocketvna_get_valid_frequency_range.restype  = ReturnType
    
def get_valid_frequency_range(handle):    
    start = Frequency(0)    
    end   = Frequency(0)

    r = PVNA.pocketvna_get_valid_frequency_range(handle, byref(start), byref(end))

    check_OK(r, "Failed to get Allowed Frequency Range")

    return start.value, end.value

#     PVNA_Res   pocketvna_get_reasonable_frequency_range(const PVNA_DeviceHandler handle, PVNA_Frequency * from, PVNA_Frequency *to);

PVNA.pocketvna_get_reasonable_frequency_range.argtypes = [DeviceHandler, FrequencyPtr, FrequencyPtr]
PVNA.pocketvna_get_reasonable_frequency_range.restype  = ReturnType
    
def get_reasonable_frequency_range(handle):    
    start = Frequency(0)    
    end   = Frequency(0)

    r = PVNA.pocketvna_get_reasonable_frequency_range(handle, byref(start), byref(end))

    check_OK(r, "Failed to get Reasonable Frequency Range")

    return start.value, end.value

#     PVNA_Res   pocketvna_single_query(const PVNA_DeviceHandler handle, const PVNA_Frequency frequency, const uint16_t average,
#                                           const PVNA_NetworkParam params,
#                                           PVNA_SParam * s11,  PVNA_SParam * s21,
#                                           PVNA_SParam * s12,  PVNA_SParam * s22);
# -------------------------------------
PVNA.pocketvna_single_query.argtypes = [DeviceHandler, Frequency, Average,
                                            NetworkParam,
                                            Priv_SParamRef,  Priv_SParamRef,
                                            Priv_SParamRef,  Priv_SParamRef ]
PVNA.pocketvna_single_query.restype  = ReturnType

def scan_frequency(handle, freq, avg, netparams):
    s11,  s21 = Priv_SParam(0.0, 0.0),  Priv_SParam(0.0, 0.0)
    s12,  s22 = Priv_SParam(0.0, 0.0),  Priv_SParam(0.0, 0.0)

    r = PVNA.pocketvna_single_query(handle, Frequency(freq), c_uint16(avg),
                                         NetworkParam(netparams), 
                                         byref(s11), byref(s21),
                                         byref(s12), byref(s22)
    )

    check_OK(r, 'Failed to scan for frequency {}'.format(freq))


    return complex(s11.real, s11.imag), complex(s21.real, s21.imag), complex(s12.real, s12.imag),  complex(s22.real, s22.imag)

#     PVNA_Res   pocketvna_multi_query(const PVNA_DeviceHandler handle, const PVNA_Frequency * frequencies, const uint32_t size,
#                                         const uint16_t average, const PVNA_NetworkParam params,
#                                         PVNA_SParam * s11a, PVNA_SParam * s21a,
#                                         PVNA_SParam * s12a, PVNA_SParam * s22a,
#                                         PVNA_ProgressCallBack * progress);
def zeros_Priv_SParams(size):
    return numpy.zeros(size * 2, dtype=numpy.float64)

def to_ptr(numpy_Priv_SParams):
    return numpy_Priv_SParams.ctypes.data_as(POINTER(c_double))

def freq2ptr(numpy_Priv_SParams):
    return numpy_Priv_SParams.ctypes.data_as(FrequencyPtr)

def complex_at(sa, idx):
    return complex(sa[idx * 2], sa[idx * 2 + 1])

def zeros_complex(size):
    return numpy.zeros(size, dtype=numpy.complex128)

PVNA.pocketvna_multi_query.argtypes = [DeviceHandler, FrequencyPtr, c_uint32,
                                            Average,  NetworkParam,
                                            Priv_SParamArray, Priv_SParamArray,
                                            Priv_SParamArray, Priv_SParamArray,
                                            c_void_p]
PVNA.pocketvna_multi_query.restype  = ReturnType

def scan_frequencies(handle, freqs, avg, netparams):
    freq   = numpy.array(freqs)
    size   = freq.size

    s11, s21      = zeros_Priv_SParams(size), zeros_Priv_SParams(size)
    s12, s22      = zeros_Priv_SParams(size), zeros_Priv_SParams(size)

    r = PVNA.pocketvna_multi_query(handle, freq2ptr(freq), c_uint32(size),
                                   Average(avg), NetworkParam(netparams),
                                   to_ptr(s11), to_ptr(s21), 
                                   to_ptr(s12), to_ptr(s22),
                                   None
    )

    check_OK(r, "Failed to scan a set of frequencies")
    
    rs11, rs21 = zeros_complex(size), zeros_complex(size)
    rs12, rs22 = zeros_complex(size), zeros_complex(size)

    for idx in range(0, size ):
        rs11[idx], rs21[idx] = complex_at(s11, idx),  complex_at(s21, idx)
        rs12[idx], rs22[idx] = complex_at(s12, idx),  complex_at(s22, idx)

    return rs11, rs21, rs12, rs22

#     PVNA_Res   pocketvna_enter_dfu_mode(const PVNA_DeviceHandler handle);
PVNA.pocketvna_enter_dfu_mode.argtypes = [DeviceHandler]
PVNA.pocketvna_enter_dfu_mode.restype  = ReturnType
def enter_dfu_mode(handle):
    r = PVNA.pocketvna_enter_dfu_mode(handle)

    check_OK(r, "Could not enter Device Firmware Update mode")

    return True

#     PVNA_Res   pocketvna_debug_request(const PVNA_DeviceHandler handle, uint8_t * buffer, uint32_t * size);
PVNA.pocketvna_debug_request.argtypes = [DeviceHandler, POINTER(c_uint8), POINTER(c_uint32)]
PVNA.pocketvna_debug_request.restype  = ReturnType

def debug_testrequest(handle):
    buff  = numpy.zeros(10, dtype=numpy.uint8)
    buffpt=buff.ctypes.data_as(POINTER(c_uint8))
    size  = c_uint32(buff.size)

    r = PVNA.pocketvna_debug_request(handle, buffpt, byref(size))

    check_OK(r, "Test request has been failed")

    return buff, size.value

#     PVNA_Res   pocketvna_debug_read_buffer(const PVNA_DeviceHandler handle, uint8_t * buffer, uint32_t * size);
PVNA.pocketvna_debug_read_buffer.argtypes = [DeviceHandler, POINTER(c_uint8), POINTER(c_uint32)]
PVNA.pocketvna_debug_read_buffer.restype  = ReturnType

def debug_readbuffer(handle, s):
    buff  = numpy.zeros(s, dtype=numpy.uint8)
    buffpt=buff.ctypes.data_as(POINTER(c_uint8))
    size  = c_uint32(buff.size)

    r = PVNA.pocketvna_debug_read_buffer(handle, buffpt, byref(size))

    check_OK(r, "Failed to read device internal buffer")

    return buff, size.value

# //-------------- Query stuff ------------------------------------

#     PVNA_Res   pocketvna_debug_read_buffer(const PVNA_DeviceHandler handle, uint8_t * buffer, uint32_t * size);

class Driver:    
    def enumerate(self):
        if self.list is not None:  
            free_list(self.list)
            self.list, self.size = [], 0

        self.list, self.size = list_devices()

        return self.list, self.size

    def count(self):
        return self.size

    def info_at(self, idx):
        if self.size == 0 or self.size <= idx:
            return dict({})

        desc = self.list[idx]

        return dict({"index": idx, "path:": desc.path, "version": desc.version, "SN": desc.sn, "product": desc.product, "vendor": desc.vendor, 
                "read": bool(desc.access&Access.Read), "write": bool(desc.access&Access.Write)
        })

    def valid(self):
        if not bool(self.handle): return False

        if is_valid(self.handle):
            return True
        else:
            self.handle = None
            return False

    def safe_connect_to(self, idx):
        try:
            return connect_to(idx)
        except PocketVnaAPIError:
            return False
        except:
            return None

    def connect_to(self, idx):
        if self.size == 0 or self.size <= idx:
            return False

        try:
            if self.valid():
                release_handler(self.handle)
                self.handle, self.info   = None, {}
        except PocketVnaHandlerInvalid:
            self.handle, self.info   = None, {}
        except:
            self.handle, self.info   = None, {}
            raise

        try:
            self.info   = self.info_at(idx)
            self.handle = get_device_handler(self.list[idx])
            return True
        except PocketVnaAccessDenied:
            i = self.info
            self.handle, self.info   = None, {}
            raise PocketVnaAccessDenied('{}'.format(i))
        except:
            self.handle, self.info   = None, {}
            raise

    def version(self):
        if not bool(self.handle): return None

        try:
            return get_version(self.handle)
        except PocketVnaHandlerInvalid:
            self.handle, self.info   = None, {}
            raise            

    def Z0(self):
        if not bool(self.handle): return None

        try:
            return get_characteristic_impedance(self.handle)
        except PocketVnaHandlerInvalid:
            self.handle, self.info   = None, {}
            raise

    def valid_frequency_range(self):
        if not bool(self.handle): return None

        try:
            return get_valid_frequency_range(self.handle)
        except PocketVnaHandlerInvalid:
            self.handle, self.info   = None, {}
            raise

    def reasonable_frequency_range(self):
        if not bool(self.handle): return None

        try:
            return get_reasonable_frequency_range(self.handle)
        except PocketVnaHandlerInvalid:
            self.handle, self.info   = None, {}
            raise

    def has_network_param(self, Priv_SParam):
        if not bool(self.handle): return None

        try:
            return supports_network_parameter(self.handle, Priv_SParam)
        except PocketVnaHandlerInvalid:
            self.handle, self.info   = None, {}
            raise

    def has_s11(self):
        return self.has_network_param(NetworkParams.S11)        

    def has_s21(self):
        return self.has_network_param(NetworkParams.S21)

    def has_s12(self):
        return self.has_network_param(NetworkParams.S12)

    def has_s22(self):
        return self.has_network_param(NetworkParams.S22)

    def close(self):
        try:
            release_handler(self.handle)
            free_list(self.list)
        except PocketVnaHandlerInvalid:
            self.handle, self.info   = None, {}
            self.list, self.size = [], 0
        except:
            self.handle, self.info   = None, {}
            self.list, self.size = [], 0
            raise

    def single_scan(self, freq, avg, netparams):
        if not bool(self.handle): return None

        try:
            return scan_frequency(self.handle, freq, avg, netparams)
        except PocketVnaHandlerInvalid:
            self.handle, self.info   = None, {}
            raise

    def scan(self, freqs, avg, netparams):
        if freqs.size < 1: return [], [], [], []
        if not bool(self.handle): return None
        return scan_frequencies(self.handle, freqs, avg, netparams)

    def __init__(self):
        self.list, self.handle, self.size = None, None, 0
        self.enumerate()

    def dfu_mode(self):
        if not bool(self.handle): return None

        try:
            return enter_dfu_mode(self.handle)
        except PocketVnaHandlerInvalid:
            self.handle, self.info   = None, {}
            raise

    def test_req(self):
        if not bool(self.handle): return None

        try:
            return debug_testrequest(self.handle)

        except PocketVnaHandlerInvalid: 
            self.handle, self.info   = None, {}
            raise           


    def read_internal_buffer(self):   
        if not bool(self.handle): return None     

        try:
            should_be_enough = 200
            return debug_readbuffer(self.handle, 200)
        except PocketVnaHandlerInvalid:
            self.handle, self.info   = None, {}
            raise  

