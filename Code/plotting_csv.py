import csv
import matplotlib.pyplot as plt
import numpy as np

file_names = ['no_phaseshifter.csv', '8GHz_phase_shift_0.csv', '8GHz_phase_shift_1.csv',
	'8GHz_phase_shift_90.csv', '8GHz_phase_shift_180.csv', '8GHz_phase_shift_360.csv']

save_folder = "C:/Users/Jacco/Documents/GitHub/bep-jacco-van-leenen/Code/img/"

freq = np.zeros(600)
s21_real = np.zeros(600)
s21_imag = np.zeros(600)
s21 = np.zeros(600)
s21_phase = np.zeros(600)

count = 0

plt.rcParams['figure.figsize'] = (10,6)

def plotting_s21(file):
	plt.figure(13)
	plt.plot(freq, s21, label=file)
	plt.grid()
	plt.legend()
	plt.xlabel("Frequency (Hz)")
	plt.ylabel("Amplitude (dB)")

def plotting_phase(file):
	plt.figure(14)
	plt.plot(freq, s21_phase, label=file)
	plt.grid()
	plt.legend()
	plt.xlabel("Frequency (Hz)")
	plt.ylabel("Amplitude (dB)")

	for i in range(1, 13):
		part = int(600/12)
		plt.figure(i)
		#print("(i-1)*part:i*part = {}:{}".format((i-1)*part,i*part))
		plt.plot(freq[(i-1)*part:i*part], s21_phase[(i-1)*part:i*part], '.-', label=file)
		plt.legend()
		plt.xlabel("Frequency (Hz)")
		plt.ylabel("Amplitude (dB)")
		plt.grid()

for file in file_names:
	count = 0
	with open(file, 'r') as csv_file:
		csv_reader = csv.reader(csv_file, delimiter=";")
		next(csv_reader)

		for line in csv_reader:
			print(line[0])
			freq[count] = line[0]
			s21[count] = line[21]
			s21_phase[count] = line[22]

			count += 1

	plotting_s21(file)
	plotting_phase(file)

for j in range(1, 15):
	save_file = save_folder + "8GHz_{}".format(j)
	plt.figure(j)
	plt.savefig(save_file)


plt.show()
plt.close()


