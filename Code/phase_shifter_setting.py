from stlab.devices.Vaunix_Phase import Vaunix_Phase

ph1 = Vaunix_Phase(19164)
print("Phase: {}".format(ph1.GetPhase()))
print("Frequency: {}".format(ph1.GetFrequency()))

ph1.SetPhase(360)
ph1.SetFrequency(8000000000)
print("Phase: {}".format(ph1.GetPhase()))
print("Frequency: {}".format(ph1.GetFrequency()))
