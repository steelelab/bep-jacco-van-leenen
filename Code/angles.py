import matplotlib.pyplot as plt
import numpy as np

# Constants
N = 36
B = 1/2
C = 0.5*B
step_size = 2*np.pi/N

#array of phase with spacing of step_size
rot = np.linspace(0, 2*np.pi, N+1)

# Make angle array
angle = np.zeros(N+1)

# Find the angle
# Probably can do this without for loop
for n in range(N+1):
    A_real = B*np.cos(0) + C*np.cos(n*step_size)
    A_imag = B*np.sin(0) + C*np.sin(n*step_size)
    angle[n] = np.arctan2(A_imag, A_real)
angle_d = angle*180/np.pi

# Plotting my evil plans
plt.plot(rot, angle_d, '.-')
plt.title("Phase of resulting curve in degrees for different $\phi_2 = \phi_1 \pm $step_size")
plt.xlabel("radians")
plt.ylabel("degrees")
plt.show()
