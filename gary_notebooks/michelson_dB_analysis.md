---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.3.0
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python
import numpy as np
import matplotlib.pyplot as plt
```

```python
plt.rcParams['figure.figsize'] = (6,6)
plt.rcParams['figure.dpi'] = 300
```

# Illustrative 10 degree steps

```python
N=36
th = np.linspace(0,2*np.pi,N)
th1, th2 = np.meshgrid(th,th)
th1 = np.reshape(th1,N*N)
th2 = np.reshape(th2,N*N)
x = (np.cos(th1) + np.cos(th2))/2
y = (np.sin(th1) + np.sin(th2))/2
```

```python
plt.plot(x,y,'.-')
```

```python
M=100
l = np.linspace(-1,1,M)
X,Y = np.meshgrid(l,l)
xg = np.reshape(X,M*M)
yg = np.reshape(Y,M*M)
ind = np.where((xg**2+yg**2<0.9**2)*(xg**2+yg**2 > 0.25**2))
xg = xg[ind]
yg = yg[ind]
```

```python
plt.plot(x,y,'.')
plt.plot(xg,yg,'.',c='k',ms=1)
```

```python
min_sq_dist = np.zeros(len(xg))
for i in range(len(xg)):
    min_sq_dist[i] = np.min((x-xg[i])**2 + (y-yg[i])**2)
```

```python
plt.figure(figsize=(18,6))
plt.plot(min_sq_dist/(xg**2+yg**2),'.')
plt.yscale('log')
plt.grid()
```

```python
np.log10(np.max(min_sq_dist/(xg**2+yg**2)))*10
```

About 15 dB or so with 10 degree steps. 


# Realistic: 1 degree steps

```python
N=360
th = np.linspace(0,2*np.pi,N)
th1, th2 = np.meshgrid(th,th)
th1 = np.reshape(th1,N*N)
th2 = np.reshape(th2,N*N)
x = (np.cos(th1) + np.cos(th2))/2
y = (np.sin(th1) + np.sin(th2))/2
```

```python
M=100
l = np.linspace(-1,1,M)
X,Y = np.meshgrid(l,l)
xg = np.reshape(X,M*M)
yg = np.reshape(Y,M*M)
ind = np.where((xg**2+yg**2<0.9)*(xg**2+yg**2 > 0.1))
xg = xg[ind]
yg = yg[ind]
```

```python
min_sq_dist = np.zeros(len(xg))
for i in range(len(xg)):
    min_sq_dist[i] = np.min((x-xg[i])**2 + (y-yg[i])**2)
```

```python
plt.figure(figsize=(18,6))
plt.plot(min_sq_dist/(xg**2+yg**2),'.')
plt.yscale('log')
plt.grid()
```

```python
np.log10(np.max(min_sq_dist/(xg**2+yg**2)))*10
```

1 degree steps give best case 37 dB.

To do: the hybrid case. 
